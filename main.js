'use strict';

requirejs([
	'graph',
	'helper',
	'underscore',
	'json!/WWW/assets/js/har/desktop-home.har.perf.json'
],
function (Graph, helper) {

	var Options,
		options,
		mainUrl = 'www.main.com.br';

	Options = function() {
		this.o = {
			selector: "#oneColor",
			highcharts: {
				chart: {
					type: 'bar'
				},
				xAxis: {
					// plotLines: {
					// 	value: 10,
					// },
					type: 'category',
					labels: {
						formatter: function () {
							if (this.value === mainUrl) {
								return '<span style="fill: white;">' + this.value + '</span>';
							} else {
								return this.value;
							}
						}
					},
				},
				legend: {
					enabled: false
				},
				series: [{
					name: 'series.name.time',
					colorByPoint: true,
					//data: data.concat([]),
					data: helper.mockData(),
					dataLabels: {
						enabled: true,
					}
				}]
			}
		};
		this.o.highcharts.series[0].data.sort(function(a, b) {
			if (a[1] > b[1]) {
				return 1;
			} else if (a[1] < b[1]) {
				return -1;
			}
			return 0;
		});
	};

	// data.forEach(function(v) {
	// 	values.push(v.value);
	// 	urls.push(v.url);
	// });

	// options.highcharts = {
	// 	chart: {
	// 		type: 'bar'
	// 	},
	// 	subtitle: {
	// 		text: 'in seconds'
	// 	},
	// 	// xAxis: {
	// 	// 	type: 'category',
	// 	// },
	// 	// yAxis: {
	// 	// 	min: 0,
	// 	// 	labels: {
	// 	// 		formatter: function () {
	// 	// 			if (this.value === mainUrl) {
	// 	// 				return '<span style="fill: white;">' + this.value + '</span>';
	// 	// 			} else {
	// 	// 				return this.value;
	// 	// 			}
	// 	// 		}
	// 	// 	},
	// 	// },
	// 	// legend: {
	// 	// 	enabled: false
	// 	// },
	// 	//series: series


	// };

	options = new Options();

	options.o.highcharts.colors = ['DarkCyan'];

	//options.o.highcharts.series[0].data = options.o.highcharts.series[0].data.concat([]);

	new Graph(options.o);

	//options.o.highcharts.series[0].data = options.o.highcharts.series[0].data.concat([]);

	options = new Options();

	//options.o.highcharts.series[0].data = options.o.highcharts.series[0].data.concat([]);

	options.o.selector = '#defaultColor';
	delete options.o.highcharts.colors;

	new Graph(options.o);

	//console.log('done');

	// options = new optionsModel();

	// options.o.highcharts.colors = ['red'];

	// // options = _.extend(optionsModel, {
	// // 	selector: '#defaultColor'
	// // });

	// new Graph(options.o);


	// options = new optionsModel();

	// options.o.highcharts.colors = ['black'];

	// // options = _.extend(optionsModel, {
	// // 	selector: '#defaultColor'
	// // });

	// new Graph(options.o);

	// options = _.extend(optionsModel, {
	// 	selector: '#greenRedYellowGradient',
	// 	dataGradientColors: 'green-yellow-red'
	// });

	options = new Options();

	options.o.selector = '#greenRedYellowGradient';
	options.o.dataGradientColors = 'green-yellow-red';
	delete options.o.highcharts.colors;

	new Graph(options.o);



	options = new Options();

	options.o.selector = '#pieChart1';
	options.o.highcharts.chart.type = 'pie';
	delete options.o.highcharts.colors;

	new Graph(options.o);



	// options = _.extend(optionsModel, {
	// 	selector: '#pieChart1'
	// });
	// options.highcharts.series[0] = _.extend(optionsModel.highcharts.series[0], {
	// 	type: 'pie'
	// });

	// new Graph(options);


	//new Graph(options);
});